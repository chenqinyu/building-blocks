package common

import (
	"fmt"
	"github.com/shirou/gopsutil/cpu"
	"github.com/shirou/gopsutil/mem"
	"sync"
	"time"
)

type Monitor struct {
	mux           *sync.Cond
	done          bool
	CpuProportion float64
	MemProportion float64
	monitorItem   *MonitorItem
	monitorFunc   []func(mi MonitorItemInterface)
}

type MonitorItemInterface interface {
	GetCpu() float64
	GetMem() float64
	GetWeight() int
}

type MonitorItem struct {
	mi  *Monitor
	cpu float64
	mem float64
}

func (mi *MonitorItem) GetCpu() float64 {
	return mi.cpu
}

func (mi *MonitorItem) GetMem() float64 {
	return mi.mem
}

func (mi *MonitorItem) GetWeight() int {
	return int(mi.mem*mi.mi.MemProportion) + int(mi.cpu*mi.mi.CpuProportion)
}

func (c *Monitor) watch() {
	ticker := time.NewTicker(time.Second * 2)
	for {
		select {
		case <-ticker.C:
			var (
				err        error
				CpuPercent []float64
				MemPercent *mem.VirtualMemoryStat
			)
			c.mux.L.Lock()
			for !c.done {
				c.mux.Wait()
			}
			if CpuPercent, err = cpu.Percent(time.Second, false); err != nil {
				continue
			}
			if MemPercent, err = mem.VirtualMemory(); err != nil {
				continue
			}
			c.monitorItem.cpu = CpuPercent[0]
			c.monitorItem.mem = MemPercent.UsedPercent
			c.mux.L.Unlock()
			for _, v := range c.monitorFunc {
				v(c.monitorItem)
			}
		}
	}
}

func (c *Monitor) GetMonitorItemInfo() (cpu float64, mem float64) {
	return c.monitorItem.cpu, c.monitorItem.mem
}

func (c *Monitor) OpenMonitor() *Monitor {
	go c.watch()
	return c
}

func (c *Monitor) MonitorFunc(fs ...func(mi MonitorItemInterface)) *Monitor {
	if !c.done {
		c.mux.L.Lock()
		c.done = true
		c.mux.L.Unlock()
		c.mux.Broadcast()
	}
	c.monitorFunc = append(c.monitorFunc, fs...)
	return c
}

func (c *Monitor) SetProportion(CpuProportion float64, MemProportion float64) *Monitor {
	c.mux.L.Lock()
	defer c.mux.L.Unlock()
	if CpuProportion+MemProportion > 10 {
		c.CpuProportion = 1
		c.MemProportion = 1
	}
	c.CpuProportion = CpuProportion / 10
	c.MemProportion = MemProportion / 10
	fmt.Println(c.CpuProportion, c.MemProportion)
	return c
}

func NewMonitor() *Monitor {
	mi := &Monitor{
		monitorFunc: make([]func(mi MonitorItemInterface), 0, 10),
		mux:         sync.NewCond(&sync.Mutex{}),
	}
	mi.monitorItem = &MonitorItem{mi: mi}
	return mi
}
