package common

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
)

var (
	GITHUB = OAuthType{
		UrlType:  "github",
		tokenUri: "https://github.com/login/oauth/access_token",
		userUri:  "https://api.github.com/user",
	}
)

type OAuthType = struct {
	UrlType  string
	tokenUri string
	userUri  string
}

type OAuthInterface interface {
	GetToken(code string) (*Token, error)
}

type OAuthConfig struct {
	OAuthType    OAuthType
	ClientID     string
	ClientSecret string
	RedirectUrl  string
}

type Token struct {
	OAuthType   *OAuthType
	AccessToken string `json:"access_token"`
	TokenType   string `json:"token_type"`
	Scope       string `json:"scope"`
}

func (t *Token) GetUserInfo() (map[string]interface{}, error) {
	return GetUserInfo(*t.OAuthType, t)
}

func NewOAuthConfig(OAuthType OAuthType, clientId string, clientSecret string, redirectUrl string) (OAuthInterface, error) {
	if ok := inspectOAuthType(OAuthType); !ok {
		return nil, errors.New("url_type is error")
	}
	return &OAuthConfig{
		OAuthType:    OAuthType,
		ClientID:     clientId,
		ClientSecret: clientSecret,
		RedirectUrl:  redirectUrl,
	}, nil
}

func (o *OAuthConfig) GetToken(code string) (*Token, error) {
	var (
		url string
		err error
	)
	if url, err = o.GetTokenAuthUrl(code); err != nil {
		return nil, err
	}

	var res *http.Response
	var token Token
	if res, err = requestUrl(url, func(header http.Header) {
		header.Set("accept", "application/json")
	}); err != nil {
		return nil, err
	}

	if err = json.NewDecoder(res.Body).Decode(&token); err != nil {
		return nil, err
	}

	token.OAuthType = &o.OAuthType
	return &token, err
}

func (o *OAuthConfig) GetTokenAuthUrl(code string) (string, error) {
	if ok := inspectOAuthType(o.OAuthType); !ok {
		return "", errors.New("url_type is error")
	}
	return fmt.Sprintf(
		"%s?client_id=%s&client_secret=%s&code=%s",
		o.OAuthType.tokenUri, o.ClientID, o.ClientSecret, code,
	), nil
}

func GetUserInfo(OAuthType OAuthType, token *Token) (map[string]interface{}, error) {
	var (
		ok  bool
		err error
	)
	if ok = inspectOAuthType(OAuthType); !ok {
		return nil, errors.New("url_type is error")
	}
	var res *http.Response
	if res, err = requestUrl(OAuthType.userUri, func(header http.Header) {
		header.Set("accept", "application/json")
		header.Set("Authorization", fmt.Sprintf("token %s", token.AccessToken))
	}); err != nil {
		return nil, err
	}

	var userInfo = make(map[string]interface{})
	if err = json.NewDecoder(res.Body).Decode(&userInfo); err != nil {
		return nil, err
	}
	return userInfo, nil
}

func inspectOAuthType(OAuthType OAuthType) bool {
	switch OAuthType {
	case GITHUB:
		return true
	default:
		return false
	}
	return true
}

type HeaderSet func(header http.Header)

func requestUrl(url string, headerSet ...HeaderSet) (*http.Response, error) {
	var (
		req *http.Request
		err error
	)

	if req, err = http.NewRequest(http.MethodGet, url, nil); err != nil {
		return nil, err
	}

	for _, v := range headerSet {
		v(req.Header)
	}

	var httpClient = http.Client{}
	var res *http.Response

	if res, err = httpClient.Do(req); err != nil {
		return nil, err
	}

	return res, err
}
