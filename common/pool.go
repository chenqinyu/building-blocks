package common

import (
	"sync"
)

type Pool struct {
	mux  sync.RWMutex
	pool map[string]interface{}
}

func (p *Pool) Add(key string, value interface{}) {
	p.mux.Lock()
	defer p.mux.Unlock()
	p.pool[key] = value
}

func (p *Pool) Get(key string) (val interface{}, ok bool) {
	p.mux.RLock()
	defer p.mux.RUnlock()
	val, ok = p.pool[key]
	return
}

func NewPool() *Pool {
	return &Pool{
		pool: make(map[string]interface{}),
	}
}
