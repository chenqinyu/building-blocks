package etcd

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/nacos-group/nacos-sdk-go/v2/inner/uuid"
	"go.etcd.io/etcd/api/v3/mvccpb"
	clientv3 "go.etcd.io/etcd/client/v3"
	"golang.org/x/net/context"
	"log"
	"strings"
	"sync"
	"time"
)

const (
	Interval         = 3
	DefaultLeaseTime = 10
	Heartbeat        = 5
)

type Register struct {
	client3   *clientv3.Client
	stop      chan bool
	interval  int64
	leaseTime int64
	preKey    string
	localNode *LocalNode
	pool      *RegPool
	leaseID   clientv3.LeaseID
}

type LocalNode struct {
	heartbeat    int64
	lease        clientv3.Lease
	localNodeKey string
	leaseID      clientv3.LeaseID
}

func (l *LocalNode) Keep(interval int64) {
	ticker := time.NewTicker(time.Duration(l.heartbeat) * time.Second)

	for {
		select {
		case <-ticker.C:
			var (
				ctx      context.Context
				err      error
				keepChan <-chan *clientv3.LeaseKeepAliveResponse
			)
			ctx, _ = context.WithTimeout(context.Background(), time.Duration(interval)*time.Second)
			if keepChan, err = l.lease.KeepAlive(ctx, l.leaseID); err != nil {
				continue
			}
			for resp := range keepChan {
				log.Println("keep the heartbeat: resp.ID", resp.ID)
			}
		}
	}
}

type Option func(info *NodeInfo)

func SetWeight(weight int) Option {
	return func(info *NodeInfo) {
		info.Weight = weight
	}
}

type RegPool struct {
	mux        sync.RWMutex
	optimalKey string
	data       map[string]NodeInfo
}

type NodeInfo struct {
	Addr    string      `json:"addr"`
	SvrID   string      `json:"svr_id"`
	SvrName string      `json:"svr_name"`
	Weight  int         `json:"weight"`
	Config  interface{} `json:"config"`
}

func NewRegister(client *clientv3.Client, prefix string) *Register {
	return &Register{
		client3:   client,
		interval:  Interval,
		leaseTime: DefaultLeaseTime,
		preKey:    getRegKeyPrefix(prefix),
		stop:      make(chan bool, 1),
		localNode: &LocalNode{
			heartbeat: Heartbeat,
		},
		pool: &RegPool{data: make(map[string]NodeInfo)},
	}
}

func getRegKeyPrefix(prefix string) string {
	return fmt.Sprintf("%s/%s", prefix, "services")
}

func getRegKey(svrName string, svrID string) string {
	return fmt.Sprintf("%s%s", svrName, svrID)
}

func randomStr(len int) (string, error) {
	id, err := uuid.NewV4()
	if err != nil {
		return "", err
	}
	str := strings.Replace(id.String(), "-", "", -1)
	if len < 0 || len >= 32 {
		return str, nil
	}
	return str[:len], nil
}

func (r *Register) LocalNode(node *NodeInfo) error {
	var (
		err  error
		data []byte
	)
	if node == nil {
		return errors.New("node info is nil")
	}
	id, _ := randomStr(32)
	node.SvrID = id
	data, err = json.Marshal(node)
	if err != nil {
		return err
	}
	r.localNode.localNodeKey = getRegKey(node.SvrName, id)
	return r.grant().put(fmt.Sprintf("%v/%v", r.preKey, r.localNode.localNodeKey), string(data))
}

func (r *Register) grant() *Register {
	var (
		ctx context.Context
	)
	ctx, _ = context.WithTimeout(context.Background(), time.Duration(r.interval)*time.Second)
	resp, err := r.client3.Grant(ctx, r.leaseTime)
	if err != nil {
		return nil
	}
	r.localNode.lease = clientv3.NewLease(r.client3)
	r.localNode.leaseID = resp.ID
	go r.localNode.Keep(r.interval)
	return r
}

func (r *Register) put(key string, data string) error {
	var (
		ctx  context.Context
		err  error
		resp *clientv3.LeaseTimeToLiveResponse
	)
	if r.localNode.leaseID != -1 {
		ctx, _ = context.WithTimeout(context.Background(), time.Duration(r.interval)*time.Second)
		resp, err = r.localNode.lease.TimeToLive(ctx, r.localNode.leaseID)
		if resp.TTL != -1 {
			_, err = r.client3.Put(ctx, key, data, clientv3.WithLease(r.localNode.leaseID))
		} else {
			r.localNode.leaseID = -1
		}
	}
	return err
}

func (r *Register) NodeUpdateConfiguration(config interface{}, opt ...Option) error {

	if nodeInfo, err := r.GetNode("/" + r.localNode.localNodeKey); err != nil {
		return err
	} else {
		var (
			data []byte
		)
		for _, v := range opt {
			v(nodeInfo)
		}
		nodeInfo.Config = config
		data, err = json.Marshal(nodeInfo)
		if err != nil {
			return err
		}
		return r.put(fmt.Sprintf("%v/%v", r.preKey, r.localNode.localNodeKey), string(data))
	}
}

func (r *Register) OpenWatch() *Register {
	stop := make(chan bool, 1)
	go r.watch(stop)
	go r.optimal()
	return r
}

func (r *Register) CloseWatch() {
	r.stop <- true
}

func (r *Register) watch(stop <-chan bool) {
	watchChan := r.client3.Watch(context.Background(), r.preKey, clientv3.WithPrefix())
	for {
		select {
		case w := <-watchChan:
			for _, v := range w.Events {
				r.node(v.Type, v.Kv)
			}
		case <-stop:
			goto Stop
		}
	}
Stop:
	log.Println("watch is close")
}

func (r *Register) node(state mvccpb.Event_EventType, kv *mvccpb.KeyValue) {
	r.pool.mux.Lock()
	defer r.pool.mux.Unlock()
	switch state {
	case mvccpb.PUT:
		var nodeInfo = NodeInfo{}
		_ = json.Unmarshal(kv.Value, &nodeInfo)
		key := strings.Split(string(kv.Key), r.preKey)
		r.pool.data[key[1]] = nodeInfo
	case mvccpb.DELETE:
		key := strings.Split(string(kv.Key), r.preKey)
		delete(r.pool.data, key[1])
	default:

	}
}

func (r *Register) GetNode(svrName string) (*NodeInfo, error) {
	r.pool.mux.RLock()
	defer r.pool.mux.RUnlock()
	resp, ok := r.pool.data[svrName]
	if !ok {
		return nil, errors.New("nothing")
	}
	return &resp, nil
}

func (r *Register) optimal() {
	ticker := time.NewTicker(5 * time.Second)
	for {
		select {
		case <-ticker.C:
			var (
				key string
				val int
			)
			for k, v := range r.pool.data {
				if val == 0 {
					key = k
					val = v.Weight
				}
				if val > v.Weight {
					key = k
					val = v.Weight
				}
			}
			r.pool.mux.Lock()
			r.pool.optimalKey = key
			r.pool.mux.Unlock()
		}
	}
}

func (r *Register) GetAll() map[string]NodeInfo {
	r.pool.mux.RLock()
	defer r.pool.mux.RUnlock()
	return r.pool.data
}

func (r *Register) GetOptimalNodeItem() (nodeInfo NodeInfo, ok bool) {
	r.pool.mux.RLock()
	defer r.pool.mux.RUnlock()
	nodeInfo, ok = r.pool.data[r.pool.optimalKey]
	return
}
