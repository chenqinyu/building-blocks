package expand

import (
	"github.com/opentracing/opentracing-go"
	"github.com/uber/jaeger-client-go/config"
	"io"
	"time"
)

type Jaeger struct {
	jaeger opentracing.Tracer
	closer io.Closer
}

func (j *Jaeger) GetJaeger() opentracing.Tracer {
	return j.jaeger
}

func NewJaegerTracer(serviceName string, addr string, types string, param float64, ls bool, bf int64) (*Jaeger, error) {
	cfg := &config.Configuration{
		ServiceName: serviceName,
		Sampler: &config.SamplerConfig{
			Type:  types,
			Param: param,
		},
		Reporter: &config.ReporterConfig{
			BufferFlushInterval: time.Duration(bf) * time.Second,
			LogSpans:            ls,
			LocalAgentHostPort:  addr,
		},
	}

	jaegers, closer, err := cfg.NewTracer()
	return &Jaeger{
		jaeger: jaegers,
		closer: closer,
	}, err
}
