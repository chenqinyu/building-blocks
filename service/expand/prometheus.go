package expand

import (
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"log"
	"net/http"
)

func PrometheusBoot(addr string) {
	httpServer := http.NewServeMux()
	httpServer.Handle("/metrics", promhttp.Handler())

	go func() {
		var (
			err error
		)
		if err = http.ListenAndServe(addr, httpServer); err != nil {
			if err != nil {
				log.Fatal("stop")
			}
			log.Fatalf("prometheus addr: %v", addr)
		}
	}()
}
