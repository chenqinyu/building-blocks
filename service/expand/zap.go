package expand

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
)

type Logger struct {
	Logger *zap.Logger
}

type ZapLoggerConfig struct {
	Filename   string
	MaxSize    int
	MaxBackups int
	MaxAge     int
	LocalTime  bool
	Compress   bool
}

// 实例化 zap logger 日志服务
func NewZapLogger(zp *ZapLoggerConfig) *Logger {
	syncWriter := zapcore.AddSync(
		&lumberjack.Logger{
			Filename:   zp.Filename,
			MaxSize:    zp.MaxSize,
			MaxBackups: zp.MaxBackups,
			LocalTime:  true,
			MaxAge:     zp.MaxAge,
			Compress:   zp.Compress,
		},
	)

	encoder := zap.NewProductionEncoderConfig()
	encoder.EncodeTime = zapcore.ISO8601TimeEncoder

	core := zapcore.NewCore(
		zapcore.NewJSONEncoder(encoder),
		syncWriter,
		zap.NewAtomicLevelAt(zap.DebugLevel),
	)

	log := zap.New(
		core,
		zap.AddCaller(),
		zap.AddCallerSkip(1),
	)

	return &Logger{
		Logger: log,
	}
}