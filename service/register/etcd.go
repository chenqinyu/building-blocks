package register

import (
	"github.com/go-micro/plugins/v4/config/source/etcd"
	"go-micro.dev/v4/config"
	"strconv"
)

func GetEtcdConfig(host string, port int64, prefix string) (config.Config, error) {
	etcdSource := etcd.NewSource(
		etcd.WithAddress(host+":"+strconv.FormatInt(port, 10)),
		etcd.WithPrefix(prefix),
		etcd.StripPrefix(true),
	)

	conf, err := config.NewConfig()
	if err != nil {
		return conf, err
	}

	if err = conf.Load(etcdSource); err != nil {
		return nil, err
	}

	return conf, err
}
