package register

import (
	"github.com/go-micro/plugins/v4/config/source/nacos"
	"go-micro.dev/v4/config/source"
	"strconv"
	"sync"
)

type NacosConfig struct {
	DataID string
	Group  string
}

type Observer interface {
	Name() string
	Update(*source.ChangeSet)
}

type Subject interface {
	Attach(Observer)
	Detach(Observer)
	Notify()
}

type NacosSubject struct {
	mutex      sync.RWMutex
	sourceList []*NacosSouce
	observers  map[string]Observer
}

type NacosSouce struct {
	n NacosConfig
	s source.Source
}

func (n *NacosSubject) Attach(observer Observer) {
	n.mutex.Lock()
	defer n.mutex.Unlock()
	n.observers[observer.Name()] = observer
}

func (n *NacosSubject) Detach(observer Observer) {
	n.mutex.Lock()
	defer n.mutex.Unlock()
	delete(n.observers, observer.Name())
}

func (n *NacosSubject) Notify() {
	n.mutex.RLock()
	defer n.mutex.RUnlock()
	for _, v := range n.sourceList {
		go n.SourceWatch(v)
	}
}

func (n *NacosSubject) SourceWatch(ns *NacosSouce) {
	var (
		watch source.Watcher
		err   error
	)
	watch, err = ns.s.Watch()
	if err != nil {
		return
	}
	for {
		var (
			cs  *source.ChangeSet
			err error
		)
		if cs, err = watch.Next(); err != nil {
			continue
		}
		n.observers[ns.n.DataID].Update(cs)
	}
}

func NewNacosConfig(host string, port int64, nacons ...NacosConfig) Subject {
	address := host + ":" + strconv.FormatInt(port, 10)
	var sourceList []*NacosSouce
	for _, v := range nacons {
		var nc = v
		sourceInfo := nacos.NewSource(
			nacos.WithAddress([]string{address}),
			nacos.WithDataId(nc.DataID),
			nacos.WithGroup(nc.Group),
		)
		var sourceItem = &NacosSouce{
			n: nc,
			s: sourceInfo,
		}
		sourceList = append(sourceList, sourceItem)
	}

	return &NacosSubject{
		sourceList: sourceList,
		observers:  make(map[string]Observer),
	}
}
