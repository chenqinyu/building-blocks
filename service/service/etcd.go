package service

import (
	"go-micro.dev/v4/config/source"
	clientv3 "go.etcd.io/etcd/client/v3"
	"sync"
)

const (
	EtcdWatch  = "watch"
	EtcdStatic = "static"
)

type EtcdCollection interface {
	EtcdCollection(config []byte) (*clientv3.Client, error)
	GetEtcdService() (*clientv3.Client, error)
	GetConfig() interface{}
	GetAddress() []string
	ServerName() string
}

type EtcdServiceOpen struct {
	mux    *sync.Cond
	name   string
	done   bool
	mode   string
	client *clientv3.Client
	e      EtcdCollection
}

func (e *EtcdServiceOpen) Name() string {
	return e.name
}

func (e *EtcdServiceOpen) GetConfig() interface{} {
	switch e.mode {
	case EtcdStatic:
		e.mux.L.Lock()
		for !e.done {
			e.mux.Wait()
		}
		e.mux.L.Unlock()
	}
	return e.e.GetConfig()
}

func (e *EtcdServiceOpen) GetAddress() []string {
	return e.e.GetAddress()
}

func (e *EtcdServiceOpen) ServerName() string {
	return e.e.ServerName()
}

func (e *EtcdServiceOpen) GetClient() *clientv3.Client {
	switch e.mode {
	case EtcdWatch:
		e.mux.L.Lock()
		for !e.done {
			e.mux.Wait()
		}
		e.mux.L.Unlock()
	}
	return e.client
}

func (e *EtcdServiceOpen) Update(cs *source.ChangeSet) {
	e.done = false
	e.mux.L.Lock()
	var (
		err    error
		client *clientv3.Client
	)

	if client, err = e.e.EtcdCollection(cs.Data); err != nil {
		e.mux.L.Unlock()
		return
	}
	e.client = client
	e.done = true
	e.mux.L.Unlock()
	e.mux.Broadcast()
}

func WatchEtcdOpen(name string, e EtcdCollection) *EtcdServiceOpen {
	return &EtcdServiceOpen{
		mux:  sync.NewCond(&sync.Mutex{}),
		name: name,
		e:    e,
		mode: EtcdWatch,
	}
}

func GetEtcdOpen(name string, e EtcdCollection) *EtcdServiceOpen {
	var (
		err    error
		client *clientv3.Client
	)
	if client, err = e.GetEtcdService(); err != nil {
		return nil
	}
	etcdService := &EtcdServiceOpen{
		name: name,
		e:    e,
		mode: EtcdStatic,
	}
	etcdService.client = client
	return etcdService
}
