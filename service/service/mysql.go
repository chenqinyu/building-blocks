package service

import (
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	"go-micro.dev/v4/config/source"
	"sync"
)

const (
	MysqlWatch  = "watch"
	MysqlStatic = "static"
)

type MysqlCollection interface {
	MysqlCollection(config []byte) (*gorm.DB, error)
	GetMysqlService() (*gorm.DB, error)
}

type MysqlServiceOpen struct {
	mux  *sync.Cond
	name string
	done bool
	mode string
	m    MysqlCollection
	db   *gorm.DB
}

func (m *MysqlServiceOpen) Name() string {
	return m.name
}

func (m *MysqlServiceOpen) GetDB() *gorm.DB {
	switch m.mode {
	case MysqlWatch:
		m.mux.L.Lock()
		for !m.done {
			m.mux.Wait()
		}
		m.mux.L.Unlock()
	}
	return m.db
}

func (m *MysqlServiceOpen) Update(cs *source.ChangeSet) {
	m.done = false
	m.mux.L.Lock()
	var (
		err error
		db  *gorm.DB
	)

	if db, err = m.m.MysqlCollection(cs.Data); err != nil {
		m.mux.L.Unlock()
		return
	}
	m.db = db
	m.done = true
	m.mux.L.Unlock()
	m.mux.Broadcast()
}

func WatchMysqlOpen(name string, m MysqlCollection) *MysqlServiceOpen {
	return &MysqlServiceOpen{
		mux:  sync.NewCond(&sync.Mutex{}),
		name: name,
		m:    m,
		mode: MysqlWatch,
	}
}

func GetMysqlOpen(name string, m MysqlCollection) *MysqlServiceOpen {
	var (
		err error
		db  *gorm.DB
	)
	if db, err = m.GetMysqlService(); err != nil {
		return nil
	}
	mysqlService := &MysqlServiceOpen{
		name: name,
		m:    m,
		mode: MysqlStatic,
	}
	mysqlService.db = db
	return mysqlService
}
