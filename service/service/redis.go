package service

import (
	"github.com/go-redis/redis"
	"go-micro.dev/v4/config/source"
	"sync"
)

const (
	RedisWatch  = "watch"
	RedisStatic = "static"
)

type RedisCollection interface {
	RedisCollection([]byte) (*redis.Client, error)
	GetRedisService() (*redis.Client, error)
	GetConfig() interface{}
	GetAddress() string
}

type RedisServiceOpen struct {
	mux    *sync.Cond
	name   string
	done   bool
	mode   string
	on     map[string]*chan bool
	r      RedisCollection
	client *redis.Client
}

func (r *RedisServiceOpen) Name() string {
	return r.name
}

func (r *RedisServiceOpen) GetClient() *redis.Client {
	switch r.mode {
	case RedisWatch:
		r.mux.L.Lock()
		for !r.done {
			r.mux.Wait()
		}
		r.mux.L.Unlock()
	}
	return r.client
}

func (r *RedisServiceOpen) GetConfig() interface{} {
	switch r.mode {
	case RedisWatch:
		r.mux.L.Lock()
		for !r.done {
			r.mux.Wait()
		}
		r.mux.L.Unlock()
	}
	return r.r.GetConfig()
}

func (r *RedisServiceOpen) Watch(name string) chan bool {
	if r.on == nil {
		r.on = make(map[string]*chan bool)
	}
	work := make(chan bool)
	r.mux.L.Lock()
	r.on[name] = &work
	r.mux.L.Unlock()
	return work
}

func (r *RedisServiceOpen) CloseWatch(name string) {
	if _, ok := r.on[name]; ok {
		delete(r.on, name)
	}
}

func (r *RedisServiceOpen) GetAddress() string {
	return r.r.GetAddress()
}

func (r *RedisServiceOpen) Update(cs *source.ChangeSet) {
	r.done = false
	r.mux.L.Lock()
	var (
		err    error
		client *redis.Client
	)
	if client, err = r.r.RedisCollection(cs.Data); err != nil {
		r.mux.L.Unlock()
		return
	}
	r.client = client
	r.done = true
	if r.on != nil {
		for _, v := range r.on {
			*v <- true
		}
	}
	r.mux.L.Unlock()
	r.mux.Broadcast()
}

func WatchRedisOpen(name string, r RedisCollection) *RedisServiceOpen {
	return &RedisServiceOpen{
		mux:  sync.NewCond(&sync.Mutex{}),
		name: name,
		mode: RedisWatch,
		r:    r,
	}
}

func GetRedisOpen(name string, r RedisCollection) *RedisServiceOpen {
	var (
		err    error
		client *redis.Client
	)
	if client, err = r.GetRedisService(); err != nil {
		return nil
	}
	redisServer := &RedisServiceOpen{
		name: name,
		mode: RedisStatic,
		r:    r,
	}
	redisServer.client = client
	return redisServer
}
